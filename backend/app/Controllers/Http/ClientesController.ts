import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Cliente from 'App/Models/Cliente'

export default class ClientesController {

  async store({request, response}: HttpContextContract) {

    try {
      const payload = JSON.parse(String(request.raw())) as Cliente
      console.log(payload)
      const cliente = await Cliente.create(payload)

      return response.ok({message: "Cliente cadastrado com sucesso!"})
    } catch (error) {
      console.log(error)
    }

  }
}
